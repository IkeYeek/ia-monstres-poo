Exemple d'utilisation (un peu dégueu mais fonctionnel)
===
`
public void think() {
        if(game.getWorld().equals(w)) {
            Position p = game.getPlayer().getPosition();
            if(!p.equals(getPosition())) {
                LevelStateGraph currState = new LevelStateGraph(game);
                ArrayList<LevelNode> path_to_player = currState.constructPath(getPosition(), game.getPlayer().getPosition());
                game.showPath(path_to_player);
                if(path_to_player != null) {
                    if(path_to_player.size() > 1) {
                        Position nextPos = path_to_player.get(path_to_player.size() - 2).getP();
                        Direction moove = findDirectionFromPos(nextPos);
                        if(canMove(moove)) {
                            doMove(moove);
                        }
                    } else {
                        Position nextPos = path_to_player.get(path_to_player.size() - 1).getP();
                        Direction moove = findDirectionFromPos(nextPos);
                        if(moove != null && canMove(moove)) {
                            doMove(moove);
                        }
                    }
                }
            }
        }
    }
    `
