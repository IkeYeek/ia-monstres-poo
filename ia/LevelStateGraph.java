package fr.ubx.poo.game.ia;

import fr.ubx.poo.game.Direction;
import fr.ubx.poo.game.Game;
import fr.ubx.poo.game.Position;
import fr.ubx.poo.model.decor.Decor;

import java.util.*;

public class LevelStateGraph {
    private final ArrayList<LevelNode> levelNodes;
    private final Map<Position, ArrayList<LevelNode>> ready_from_pos;

    public LevelStateGraph(Game state) {
        levelNodes = new ArrayList<>();
        //On construit une liste avec tous les points de la map
        for (int i = 0; i < state.getWorld().dimension.height; i++) {
            for (int j = 0; j < state.getWorld().dimension.width; j++) {
                levelNodes.add(new LevelNode(new Position(j, i)));
            }
        }

        //On retire des points ceux sur lesquels on* ne peut pas se déplacer (* les monstres)
        Map<Position, Decor> decors = state.getWorld().getGrid();
        for (Map.Entry<Position, Decor> entry : decors.entrySet()) {
            Position p = entry.getKey();
            Decor d = entry.getValue();
            if (!d.isPickable()) {
                levelNodes.remove(new LevelNode(p));
            }
        }

        //On associe chacun des points sur lesquels on peut se déplacer à une liste de voisins directs
        Map<LevelNode, ArrayList<LevelNode>> levelNodesNeighborhoods = new HashMap<>();
        for (LevelNode e : levelNodes) {
            ArrayList<LevelNode> currLevelNodeNeighborhood = new ArrayList<>();
            for (Direction dir : Direction.values()) {
                Position np = dir.nextPosition(e.getP());
                if(levelNodes.stream().anyMatch(v -> v.getP().equals(np))) {
                    LevelNode neighbor = levelNodes.stream().filter(v -> v.getP().equals(np)).findFirst().get();
                    currLevelNodeNeighborhood.add(neighbor);
                }
            }
            levelNodesNeighborhoods.put(e, currLevelNodeNeighborhood);
        }
        levelNodesNeighborhoods.forEach(LevelNode::setNeighbors);

        ready_from_pos = new HashMap<>();
    }

    /*
    Colors:
    0 - BLANC
    1 - NOIR
     */
    public void dijkstra(Position from_p) {
        ArrayList<LevelNode> levelNodes = new ArrayList<>(this.levelNodes);
        for (LevelNode levelNode : levelNodes) {
            levelNode.setDist(Integer.MAX_VALUE);
            levelNode.setF(null);
            levelNode.setColor(0);
        }
        Optional<LevelNode> maybeFrom = levelNodes.stream().filter(e -> e.getP().equals(from_p)).findFirst();
        if(maybeFrom.isEmpty()) {
            ready_from_pos.put(from_p, null);
            return;
        }
        LevelNode from = maybeFrom.get();
        from.setDist(0);
        PriorityQueue<LevelNode> levelNodePriorityQueue = new PriorityQueue<>();
        levelNodePriorityQueue.add(from);
        while(!levelNodePriorityQueue.isEmpty()) {
            LevelNode pivot = levelNodePriorityQueue.poll();
            for (LevelNode e : pivot.getNeighbors()) {
                if (e.getColor() == 0) {
                    if (e.getDist() == Integer.MAX_VALUE) {
                        levelNodePriorityQueue.add(e);
                    }
                    if (e.getDist() > pivot.getDist()) {
                        e.setDist(pivot.getDist() + 1);
                        e.setF(pivot);
                    }
                }
            }
            pivot.setColor(1);
        }
        ready_from_pos.put(from_p, levelNodes);
    }

    public ArrayList<LevelNode> constructPath(Position from, Position to) {
        if(!ready_from_pos.containsKey(from)) {
            dijkstra(from);
        }
        if(ready_from_pos.get(from) == null) {
            return null;
        }
        ArrayList<LevelNode> currLevelNodes = ready_from_pos.get(from);
        Optional<LevelNode> maybe_to_node = currLevelNodes.stream().filter(e -> e.getP().equals(to)).findAny();
        if(maybe_to_node.isEmpty())
            throw new RuntimeException("Unknown position " + to);
        LevelNode to_node = maybe_to_node.get();
        ArrayList<LevelNode> path = new ArrayList<>(Collections.singletonList(to_node));

        LevelNode curr_node = to_node.getF();
        while(curr_node != null) {
            path.add(curr_node);
            curr_node = curr_node.getF();
        }

        return path;
    }

    public ArrayList<LevelNode> getLevelNodes() {
        return levelNodes;
    }
    
}
