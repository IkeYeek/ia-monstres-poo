package fr.ubx.poo.game.ia;

import fr.ubx.poo.game.Position;

import java.util.ArrayList;

public class LevelNode implements Comparable{
    private final Position p;
    private ArrayList<LevelNode> neighbors;
    private int color;
    private int dist;
    private LevelNode f = null;

    public LevelNode(Position p) {
        this.p = p;
        neighbors = new ArrayList<>();
    }

    public Position getP() {
        return p;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public int getDist() {
        return dist;
    }

    public void setDist(int dist) {
        this.dist = dist;
    }

    public LevelNode getF() {
        return f;
    }

    public void setF(LevelNode f) {
        this.f = f;
    }

    public ArrayList<LevelNode> getNeighbors() {
        return neighbors;
    }


    public void setNeighbors(ArrayList<LevelNode> neighbors) {
        this.neighbors = neighbors;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof LevelNode) {
            LevelNode lno = (LevelNode) o;
            if(getDist() < lno.getDist())
                return -1;
            else if(getDist() == lno.getDist())
                return 0;
            return 1;
        }
        throw new RuntimeException("Illegal comparison");
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof LevelNode) {
            LevelNode ln = (LevelNode) obj;
            return getP().equals(ln.getP());
        }
        return false;
    }


}
